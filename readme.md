Installation instructions
=========================

These instructions only apply for Oracle WebLogic Server 12c.

1. Export a WAR file from the project.
2. Put the WAR file in the server installation location (not a domain location) in the directory `./server/lib/console-ext`.
3. Restart the server.
